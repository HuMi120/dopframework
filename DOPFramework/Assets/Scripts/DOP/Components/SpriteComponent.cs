﻿
using Dotity;

public class SpriteComponent : Component
{
    private ComponentKey _key = ComponentKey.Sprite;
    public override ComponentKey Key => _key;
}
